import random

def get_answer():
    answer = random.randint(1, 100)
    return answer

def compute_midpoint(low, high):
    midpoint = low + (high - low) / 2
    return midpoint

def get_user_input(prompt):
    user_input = input(prompt)
    return user_input

def compare_input(user_input, midpoint):
    if user_input == 'p':
        return -1
    elif user_input == 'g':
        return 1
    elif user_input == 'e':
        return 0
    else:
        print("Entrée invalide, veuillez entrer 'p' pour plus petit, 'g' pour plus grand ou 'e' pour égal.")
        return None

def main():
    answer = get_answer()
    low = 1
    high = 100

    while True:
        midpoint = compute_midpoint(low, high)
        print(f"Est-ce que le nombre que vous avez pensé est plus petit, plus grand ou égal à {midpoint} ?")
        user_input = get_user_input("Entrez 'p' pour plus petit, 'g' pour plus grand ou 'e' pour égal : ")
        comparison_result = compare_input(user_input, midpoint)

        if comparison_result is None:
            continue

        if comparison_result == 0:
            print(f"C'est le nombre auquel j'avais pensé. Bravo ! Le nombre est {midpoint}.")
            break
        elif comparison_result == 1:
            low = midpoint + 1
        else:
            high = midpoint - 1

if __name__ == "__main__":
    main()